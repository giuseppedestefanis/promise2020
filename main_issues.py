import logging
import pickle
import warnings
from os import path
from pprint import pprint

# Gensim
import gensim
import gensim.corpora as corpora
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyLDAvis
import pyLDAvis.gensim
import seaborn as sns
import spacy
from bokeh.io import output_notebook
from bokeh.plotting import figure, output_file, save
from gensim.utils import simple_preprocess
# NLTK Stop words
from nltk.corpus import stopwords
# Get topic weights and dominant topics ------------
from sklearn.manifold import TSNE
from sqlalchemy import create_engine

from query import ISSUES_BY_PROJECT_NAME, COMMENTS_BY_ISSUE_ID

stop_words = stopwords.words('english')
stop_words.extend(
    ['from', 'subject', 're', 'edu', 'use', 'not', 'would', 'say', 'could', '_', 'be', 'know', 'good', 'go', 'get',
     'do', 'done', 'try', 'many', 'some', 'nice', 'thank', 'think', 'see', 'rather', 'easy', 'easily', 'lot', 'lack',
     'make', 'want', 'seem', 'run', 'need', 'even', 'right', 'line', 'even', 'also', 'may', 'take', 'come'])

warnings.filterwarnings("ignore", category=DeprecationWarning)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

engine = create_engine('postgresql://jira_user:jira:user@localhost:5432/jira_dataset')
DATA_DIR = './data'


def sent_to_words(sentences):
    for sent in sentences:
        # sent = re.sub('\S*@\S*\s?', '', sent)  # remove emails
        # sent = re.sub('\s+', ' ', sent)  # remove newline chars
        # sent = re.sub("\'", "", sent)  # remove single quotes
        sent = gensim.utils.simple_preprocess(str(sent), deacc=True)
        yield (sent)
    # !python3 -m spacy download en  # run in terminal once


def process_words(texts,
                  stop_words=stop_words,
                  allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'],
                  bigram_mod=None,
                  trigram_mod=None):
    """Remove Stopwords, Form Bigrams, Trigrams and Lemmatization"""
    texts = [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]
    texts = [bigram_mod[doc] for doc in texts]
    texts = [trigram_mod[bigram_mod[doc]] for doc in texts]
    texts_out = []
    nlp = spacy.load('en', disable=['parser', 'ner', 'textcat'])
    nlp.max_length = 10000000
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    # remove stopwords once more after lemmatization
    texts_out = [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts_out]
    return texts_out


def format_topics_sentences(ldamodel=None, corpus=None, texts=None):
    # Init output
    sent_topics_df = pd.DataFrame()

    # Get main topic in each document
    for i, row_list in enumerate(ldamodel[corpus]):
        if len(row_list) == 0:
            continue
        row = row_list[0] if ldamodel.per_word_topics else row_list
        if isinstance(row, tuple):
            row = [row]
        row = sorted(row, key=lambda x: (x[1]), reverse=True)
        # Get the Dominant topic, Perc Contribution and Keywords for each document
        for j, (topic_num, prop_topic) in enumerate(row):
            if j == 0:  # => dominant topic
                wp = ldamodel.show_topic(topic_num)
                topic_keywords = ", ".join([word for word, prop in wp])
                sent_topics_df = sent_topics_df.append(
                    pd.Series([int(topic_num), round(prop_topic, 4), topic_keywords]), ignore_index=True)
            else:
                break
    sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']
    # Add original text to the end of the output
    contents = pd.Series(texts)
    sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    return (sent_topics_df)


def topics_document_words_freq_plot(df_dominant_topic):
    cols = [color for name, color in mcolors.TABLEAU_COLORS.items()]  # more colors: 'mcolors.XKCD_COLORS'

    fig, axes = plt.subplots(2, 2, figsize=(16, 14), dpi=160, sharex=True, sharey=True)

    for i, ax in enumerate(axes.flatten()):
        df_dominant_topic_sub = df_dominant_topic.loc[df_dominant_topic.Dominant_Topic == i, :]
        doc_lens = [len(d) for d in df_dominant_topic_sub.Text]
        ax.hist(doc_lens, bins=1000, color=cols[i])
        ax.tick_params(axis='y', labelcolor=cols[i], color=cols[i])
        sns.kdeplot(doc_lens, color="black", shade=False, ax=ax.twinx())
        ax.set(xlim=(0, 1000), xlabel='Document Word Count')
        ax.set_ylabel('Number of Documents', color=cols[i])
        ax.set_title('Topic: ' + str(i), fontdict=dict(size=16, color=cols[i]))

    fig.tight_layout()
    fig.subplots_adjust(top=0.90)
    plt.xticks(np.linspace(0, 1000, 9))
    fig.suptitle('Distribution of Document Word Counts by Dominant Topic', fontsize=22)
    plt.show()


def visualize_topics(lda_model, corpus, project_name, including_comments):
    vis = pyLDAvis.gensim.prepare(lda_model, corpus, dictionary=lda_model.id2word, mds='mmds')
    if not including_comments:
        file_name = '{}/{}/topic_modeling_visualization_only_issues.html'.format(DATA_DIR,
                                                                                 project_name.replace('/', ' '))
    else:
        file_name = '{}/{}/topic_modeling_visualization_issues_with_comments.html'.format(DATA_DIR,
                                                                                          project_name.replace(
                                                                                              '/', ' '))
    pyLDAvis.save_html(vis, file_name)


def show_topic_clusters(lda_model, corpus, project_name, including_comments):
    topic_weights = []
    for i, row_list in enumerate(lda_model[corpus]):
        topic_weights.append([w for i, w in row_list[0]])

    # Array of topic weights
    arr = pd.DataFrame(topic_weights).fillna(0).values

    # Keep the well separated points (optional)
    arr = arr[np.amax(arr, axis=1) > 0.35]

    # Dominant topic number in each doc
    topic_num = np.argmax(arr, axis=1)

    # tSNE Dimension Reduction
    tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')
    tsne_lda = tsne_model.fit_transform(arr)

    # Plot the Topic Clusters using Bokeh
    output_notebook()
    if not including_comments:
        file_name = '{}/{}/topic_modeling_only_issues.html'.format(DATA_DIR, project_name.replace('/', ' '))
    else:
        file_name = '{}/{}/topic_modeling_issues_with_comments.html'.format(DATA_DIR, project_name.replace('/', ' '))
    output_file(file_name)

    mycolors = np.array([color for name, color in mcolors.TABLEAU_COLORS.items()])
    plot = figure(title="t-SNE Clustering of {} LDA Topics for {}".format(n_topics, project_name),
                  plot_width=900, plot_height=700)
    plot.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], color=mycolors[topic_num])
    save(plot)


def get_comment_by_issue_id(issue_id):
    df = pd.read_sql_query(COMMENTS_BY_ISSUE_ID.format(issue_id), con=engine)
    return " ".join([c for c in df['comment']])


def get_issues_by_project_name(project_name, include_comments=False, limit=1000):
    df = pd.read_sql_query(ISSUES_BY_PROJECT_NAME.format(project_name, limit), con=engine)
    print(df.shape)
    if include_comments:
        return [desc + title + get_comment_by_issue_id(issue_id)
                for issue_id, desc, title in zip(df['id'], df['description'], df['title'])]
    else:
        return ["{} {}".format(x, y) for x, y in zip(df['description'], df['title'])]


def get_lda_model(project_name, limit):
    file_name = "{}/{}/lda.model".format(DATA_DIR, project_name)
    if path.exists(file_name):
        with open(file_name, 'rb') as lda_file:
            return pickle.load(lda_file)
    else:
        lda_model, corpus, data_ready = issues_LDA(project_name, limit, False)
        with open(file_name, 'wb') as lda_file:
            pickle.dump(lda_model, lda_file)
        return lda_model


def issues_LDA(project_name, number_of_docs, including_comments, n_topics=10):
    # Import Dataset
    data = get_issues_by_project_name(project_name,
                                      include_comments=including_comments,
                                      limit=number_of_docs)
    print(len(data))
    data = data[:number_of_docs]
    data_words = list(sent_to_words(data))
    print(data_words[:1])
    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
    trigram = gensim.models.Phrases(bigram[data_words], threshold=100)
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    trigram_mod = gensim.models.phrases.Phraser(trigram)
    data_ready = process_words(data_words, bigram_mod=bigram_mod,
                               trigram_mod=trigram_mod)  # processed Text Data!
    # Create Dictionary
    id2word = corpora.Dictionary(data_ready)
    # Create Corpus: Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in data_ready]
    # Build LDA model
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                id2word=id2word,
                                                num_topics=n_topics,
                                                random_state=100,
                                                update_every=1,
                                                chunksize=10,
                                                passes=10,
                                                alpha='symmetric',
                                                iterations=100,
                                                per_word_topics=True)
    return lda_model, corpus, data_ready


if __name__ == '__main__':

    projects = [
        'Hive', 'HBase', 'Camel', 'Cassandra', 'Solr', 'Hibernate ORM',
        'Derby', 'Hadoop Common', 'Hadoop HDFS', 'Wicket', 'GeoServer',
        'Geronimo', 'Pig', 'ZooKeeper', 'Hadoop Map/Reduce', 'JRuby', 'OFBiz'

    ]
    number_of_docs = 15000
    n_topics = 10
    including_comments = False

    for project_name in projects:
        try:
            lda_model, corpus, data_ready = issues_LDA(project_name, number_of_docs, including_comments)

            pprint(lda_model.print_topics())
            df_topic_sents_keywords = format_topics_sentences(ldamodel=lda_model, corpus=corpus, texts=data_ready)

            # Format
            df_dominant_topic = df_topic_sents_keywords.reset_index()
            df_dominant_topic.columns = ['Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']
            print(df_dominant_topic.head(10))

            # Get topic weights
            show_topic_clusters(lda_model, corpus, project_name, including_comments)
            visualize_topics(lda_model, corpus, project_name, including_comments)
        except Exception as e:
            print(e)
