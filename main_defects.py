import csv
import pickle
from collections import defaultdict
from os import path

import gensim
import pandas as pd
from gensim import corpora

from main_issues import engine, DATA_DIR, issues_LDA, sent_to_words, process_words, format_topics_sentences
from query import ISSUES_BY_KEYS, DEVELPOPER_BY_NAME, DEVELPOPERS_BY_PROJECT_NAME, ISSUE_BY_KEY


def get_lda_model(project_name, limit, n_topics=10):
    file_name = "{}/{}/lda_model.dump".format(DATA_DIR, project_name)
    if path.exists(file_name):
        with open(file_name, 'rb') as lda_file:
            return pickle.load(lda_file)
    else:
        print("Evaluating LDA MODEL...")
        lda_model, corpus, data_ready = issues_LDA(project_name, limit, False, n_topics=n_topics)
        with open(file_name, 'wb') as lda_file:
            pickle.dump(lda_model, lda_file)
        return lda_model


def get_dominant_topic(lda_model, docs):
    data_words = list(sent_to_words(docs))
    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
    trigram = gensim.models.Phrases(bigram[data_words], threshold=100)
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    trigram_mod = gensim.models.phrases.Phraser(trigram)
    data_ready = process_words(data_words, bigram_mod=bigram_mod,
                               trigram_mod=trigram_mod)  # processed Text Data!
    # Create Dictionary
    id2word = corpora.Dictionary(data_ready)
    # Create Corpus: Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in data_ready]
    return format_topics_sentences(lda_model, corpus, data_ready)


def developer_dominant_topics(project_name, dev_issues, limit=1000, num_of_topics=10):
    lda_model = get_lda_model(project_name, limit=limit, n_topics=num_of_topics)
    dev_dominant_topics = [0 for i in range(0, num_of_topics)]
    topics = get_dominant_topic(lda_model, dev_issues)
    for i, topic in topics.iterrows():
        dev_dominant_topics[int(topic['Dominant_Topic'])] += 1
    for i, topic in enumerate(dev_dominant_topics):
        topic_percentage = '%.4f' % (topic / len(dev_issues)) if len(dev_issues) else 0
        dev_dominant_topics[i] = float(topic_percentage)
    return dev_dominant_topics


def get_developers_dominant_topic(project_name, dev_name_issues_dict, limit=1000, num_of_topics=10):
    file_name = "{}/{}/dev_topics_dict.dump".format(DATA_DIR, project_name)
    if path.exists(file_name):
        with open(file_name, 'rb') as dev_dict_file:
            return pickle.load(dev_dict_file)
    dev_num = len(dev_name_issues_dict.keys())
    print("Evaluating Developers Topics....")
    print("Found %d developers" % dev_num)
    i = 0
    dev_dominant_topics = defaultdict(list)
    for dev_name, issues in dev_name_issues_dict.items():
        i += 1
        print("%s %d of %d\r" % (dev_name, i, dev_num), end='', flush=True)
        defect_introducers_df = pd.read_sql_query(DEVELPOPER_BY_NAME.format(dev_name),
                                                  con=engine)
        ids = list(defect_introducers_df.id.values)
        if len(ids) > 0:
            issue_keys = str(tuple(issues)) if len(issues) > 1 else "('{}')".format(issues[0])
            dev_id = ids[0]
            issues_df = pd.read_sql_query(ISSUES_BY_KEYS.format(issue_keys, project_name, dev_id), con=engine)
            issues = [(descr or "") + (title or "") for descr, title in
                      zip(issues_df['description'], issues_df['title'])]
            dev_dominant_topics[dev_name] = developer_dominant_topics(project_name,
                                                                      issues,
                                                                      limit=limit,
                                                                      num_of_topics=num_of_topics)
    with open(file_name, 'wb') as lda_file:
        pickle.dump(dev_dominant_topics, lda_file)
    return dev_dominant_topics


def create_final_csv(project_name, analysis_name, num_of_topics, number_of_docs, issue_key_suffix=None):
    defect_introduction_df = pd.read_csv("{}/{}/bug_{}.csv".format(DATA_DIR, project_name, analysis_name),
                                         delimiter=',',
                                         encoding="ISO-8859-1",
                                         error_bad_lines=False)
    dev_name_issues_dict = defaultdict(list)
    for index, row in defect_introduction_df.iterrows():
        if row['file_name'] == 'CHANGES.txt':
            continue
        dev_name = row['name'].split("<")[0].replace("'", "''").strip()
        if not issue_key_suffix:
            issue_key = "{}-{}".format(issue_key_suffix, row['issue_key'])
        else:
            issue_key = "{}-{}".format(project_name.upper(), row['issue_key'])
        dev_name_issues_dict[dev_name] = list(set(dev_name_issues_dict[dev_name] + [issue_key]))
    dev_dominant_topics = get_developers_dominant_topic(
        project_name,
        dev_name_issues_dict,
        limit=number_of_docs,
        num_of_topics=num_of_topics
    )
    print("Writing final output....")
    dev_issue_key = defaultdict(list)
    lda_model = get_lda_model(project_name, limit=number_of_docs, n_topics=num_of_topics)
    for index, row in defect_introduction_df.iterrows():
        dev_name = row['name'].split("<")[0].replace("'", "''").strip()
        issue_key = "{}-{}".format(project_name.upper(), row['issue_key'])
        dev_issue_key[(dev_name, issue_key)] = dev_dominant_topics[dev_name]
    fieldnames = ['ISSUE_KEY', 'DEV_NAME', 'ISSUE_TOPIC']
    fieldnames += ["DEV_TOPIC_{}".format(i) for i in range(0, num_of_topics)]
    with open("{}/{}/dev_defect_{}_and_topics.csv".format(DATA_DIR, project_name, analysis_name), 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for t, topics in dev_issue_key.items():
            if len(dev_dominant_topics[t[0]]) > 0:
                row = {}
                issue_df = pd.read_sql_query(ISSUE_BY_KEY.format(t[1]), con=engine)
                issues = [(descr or "") + (title or "") for descr, title in
                          zip(issue_df['description'], issue_df['title'])]
                dominant_topic = 0
                for i, topic in get_dominant_topic(lda_model, issues).iterrows():
                    dominant_topic = int(topic['Dominant_Topic'])
                row['ISSUE_KEY'] = t[1]
                row['DEV_NAME'] = t[0]
                row['ISSUE_TOPIC'] = dominant_topic
                for i, topic in enumerate(dev_dominant_topics[t[0]]):
                    row["DEV_TOPIC_{}".format(i)] = topic
                writer.writerow(row)


if __name__ == '__main__':
    number_of_docs = 25000
    num_of_topics = 10
    project_name = 'Derby'
    analysis_name = 'introduction'
    issue_key_suffix = ''
    create_final_csv(project_name, analysis_name, num_of_topics, number_of_docs, issue_key_suffix=issue_key_suffix)
    analysis_name = 'fixing'
    create_final_csv(project_name, analysis_name, num_of_topics, number_of_docs)
    dev_i_df = pd.read_csv("./data/{}/dev_defect_introduction_and_topics.csv".format(project_name))
    dev_f_df = pd.read_csv("./data/{}/dev_defect_fixing_and_topics.csv".format(project_name))
    dev_if_df = pd.merge(dev_i_df, dev_f_df, left_on='ISSUE_KEY', right_on='ISSUE_KEY', suffixes=('_i', '_f'))
    dev_if_df['MATCH_DEV_I_F'] = dev_if_df.apply(lambda row: 1 if row.DEV_NAME_i == row.DEV_NAME_f else 0, axis=1)
    topic_i_columns = ['DEV_TOPIC_0_i', 'DEV_TOPIC_1_i', 'DEV_TOPIC_2_i', 'DEV_TOPIC_3_i', 'DEV_TOPIC_4_i',
                       'DEV_TOPIC_5_i', 'DEV_TOPIC_6_i', 'DEV_TOPIC_7_i', 'DEV_TOPIC_8_i', 'DEV_TOPIC_9_i']
    topic_f_columns = ['DEV_TOPIC_0_f', 'DEV_TOPIC_1_f', 'DEV_TOPIC_2_f', 'DEV_TOPIC_3_f', 'DEV_TOPIC_4_f',
                       'DEV_TOPIC_5_f', 'DEV_TOPIC_6_f', 'DEV_TOPIC_7_f', 'DEV_TOPIC_8_f', 'DEV_TOPIC_9_f']
    dev_if_df['DOMINANT_TOPIC_i'] = dev_if_df[topic_i_columns].idxmax(axis=1)
    dev_if_df['DOMINANT_TOPIC_f'] = dev_if_df[topic_f_columns].idxmax(axis=1)
    dev_if_df['MATCH_TOPIC_i_f'] = dev_if_df.apply(
        lambda row: 1 if row.DOMINANT_TOPIC_i[:-1] == row.DOMINANT_TOPIC_f[:-1] else 0, axis=1)
    dev_if_df = dev_if_df.drop(topic_i_columns, axis=1)
    dev_if_df = dev_if_df.drop(topic_f_columns, axis=1)
    dev_if_df = dev_if_df.drop(['ISSUE_TOPIC_f'], axis=1)
    dev_if_df.to_csv("./data/{}/dev_i_f_and_topics.csv".format(project_name))
