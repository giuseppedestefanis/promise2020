from itertools import repeat

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import PercentFormatter
import matplotlib.patches as mpatches

DATA_DIR = './data'
PROJECTS = [
    # 'Camel',
    'Hadoop Common',
    'Hadoop HDFS',
    'HBase',
    'Hive'
]


def bug_fix_introducer_plot(df):
    topic_matches = df.MATCH_TOPIC_i_f.values
    dev_matches = df.MATCH_DEV_I_F.values
    print(np.nansum(dev_matches))
    raw_data = {
        'match': ['Developer', 'Topic'],
        'i_equals_f': [np.nansum(dev_matches), np.sum(topic_matches)],
        'i_not_equals_f': [len(dev_matches) - np.nansum(dev_matches), len(topic_matches) - np.sum(topic_matches)]
    }
    df = pd.DataFrame(raw_data, columns=['match', 'i_equals_f', 'i_not_equals_f'])
    # Create a figure with a single subplot
    f, ax = plt.subplots(1, figsize=(10, 5))
    # Set bar width at 1
    bar_width = 0.5
    # positions of the left bar-boundaries
    bar_l = [i for i in range(len(df['i_equals_f']))]
    # positions of the x-axis ticks (center of the bars as bar labels)
    tick_pos = [i + (bar_width / 4) for i in bar_l]
    # Create the total score for each participant
    totals = [i + j for i, j in zip(df['i_equals_f'], df['i_not_equals_f'])]
    # Create the percentage of the total score the dev value for each participant was
    pre_rel = [i / j * 100 for i, j in zip(df['i_equals_f'], totals)]
    # Create the percentage of the total score the topic value for each participant was
    mid_rel = [i / j * 100 for i, j in zip(df['i_not_equals_f'], totals)]
    # Create a bar chart in position bar_1
    ax.bar(bar_l,
           # using pre_rel data
           pre_rel,
           # labeled
           label='Introducer=Fixer',
           # with alpha
           alpha=0.9,
           # with color
           color='b',
           # with bar width
           width=bar_width,
           # with border color
           edgecolor='white'
           )
    # Create a bar chart in position bar_1
    ax.bar(bar_l,
           # using mid_rel data
           mid_rel,
           # with pre_rel
           bottom=pre_rel,
           # labeled
           label='Introducer!=Fixer',
           # with alpha
           alpha=0.9,
           # with color
           color='r',
           # with bar width
           width=bar_width,
           # with border color
           edgecolor='white'
           )
    # Set the ticks to be first names
    plt.xticks(tick_pos, df['match'])
    ax.set_ylabel("Percentage")
    ax.set_xlabel("")
    # Let the borders of the graphic
    plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
    plt.ylim(-10, 110)
    # rotate axis labels
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
    # shot plot
    plt.legend()
    plt.savefig('topic_dev_i_f.png')
    plt.show()


def dev_fix_introducer():
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name),
                delimiter=','
            )
        )
    df = pd.concat(dfs)
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/commits.csv'.format(DATA_DIR, project_name),
                delimiter=',',
                names=['COMMIT_ID', 'DEV_NAME', 'DATE', 'COMMIT_MESSAGE'],
                encoding="ISO-8859-1",
                error_bad_lines=False
            )
        )
    df_commits = pd.concat(dfs).groupby(['DEV_NAME']).count()
    df_commits = (df_commits - df_commits.min()) / (df_commits.max() - df_commits.min())
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/users_network_edges.csv'.format(DATA_DIR, project_name),
                delimiter=',',
                encoding="ISO-8859-1",
                error_bad_lines=False
            )
        )
    df_network = pd.concat(dfs).drop(['Id', 'Label', 'timeset'], axis=1)
    df_network = (df_network - df_network.min()) / (df_network.max() - df_network.min())
    df_i = df.groupby(['DEV_NAME_i']).count()
    df_i = (df_i - df_i.min()) / (df_i.max() - df_i.min())
    df_f = df.groupby(['DEV_NAME_f']).count()
    df_f = (df_f - df_f.min()) / (df_f.max() - df_f.min())

    fig, axes = plt.subplots(1, figsize=(10, 5))
    labels = ['Defect Introduced', 'Defect Fixed',
              '# Commits', 'Betweeness', 'Page Rank',
              'Modularity Class', 'In-Degree', 'Out-Degree']
    pos = range(0, len(labels))
    f_values = df_i['DEV_NAME_f'].values
    i_values = df_f['DEV_NAME_i'].values
    c_values = df_commits['COMMIT_ID'].values
    b_values = df_network['betweenesscentrality'].values
    p_values = df_network['pageranks'].values
    m_values = df_network['modularity_class'].values
    indegree_values = df_network['indegree'].values
    outdegree_values = df_network['outdegree'].values
    min = np.min([len(f_values), len(i_values)])
    axes.violinplot(
        [f_values, i_values, c_values, b_values, p_values, m_values, indegree_values, outdegree_values], pos,
        vert=False,
    )
    axes.set_yticks(np.arange(0, len(labels)))
    axes.set_yticklabels(labels)
    fig.suptitle("Developer experience")
    fig.subplots_adjust(hspace=0.4)
    plt.savefig('dev_metrics.png')
    plt.show()
    fig, ax = plt.subplots(1, figsize=(10, 5))
    # ax = fig.add_axes([0, 0, 1, 1])
    ax.scatter(f_values[:min], i_values[:min], color='b')
    ax.set_xlabel('Bug Fixing')
    ax.set_ylabel('Bug Introduction')
    ax.set_title('Fixing vs  Introduction')
    plt.savefig('fix_vs_introduction.png')
    plt.show()


if __name__ == '__main__':
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name),
                delimiter=','
            )
        )
    df = pd.concat(dfs)
    # bug_fix_introducer_plot(df)
    dev_fix_introducer()
